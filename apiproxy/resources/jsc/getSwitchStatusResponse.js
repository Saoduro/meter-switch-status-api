var response = context.getVariable("soapResponse.body");
if (typeof response != 'undefined')
 {
	var responseBody = JSON.parse(response);
	var responseCode = responseBody.statusCode;
//	var deviceUtilId = {request.queryparam.deviceUtilId};
//	print("responseBody.statusCode");
//	print("responseCode");
	
	var responseObject = {};
	//var finalObject = {};
		
	if (responseCode === 'Success')
	{
		var correlationId = context.getVariable("correlationId");


		// map correlationId
		if (responseBody.requestId != "NULL")
		{
			responseObject.correlationId = correlationId;
		} 
		else 
		{
			responseObject.correlationId = "";
		}
		//hardcode timestamp
		var timeStamp = new Date().toISOString().replace('Z', '');
		responseObject.timestamp = timeStamp;
		
		var meter=[];
		var meterObject = {};
		//var switch={};
		
		//Fetch deviceUtilId from request parameter
		if (request.deviceUtilId != "NULL")
		{
		    
		var deviceUtilId = context.getVariable("request.queryparam.deviceUtilId");
		   
		meterObject.deviceUtilId = deviceUtilId;
		}
		else
		{
		meterObject.deviceUtilId = "";
		}
		
		
		//map meterConnectionStatus to Status
		if (responseBody.meterConnectionStatus != "NULL")
		{
			meterObject.switch = {};
			meterObject.switch.status = responseBody.meterConnectionStatus;
		}
		else
		{
			meterObject.switch = {};
			meterObject.switch.status = "";
		}
		
		
		//construct JSON response message
		//meter.push(meterObject);
		//responseObject.meter = meter;
		responseObject.meter = meterObject;
		
		}
    context.setVariable("finalResponse", JSON.stringify(responseObject));
}